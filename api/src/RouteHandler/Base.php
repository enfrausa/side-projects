<?php
    namespace App\RouteHandler;

    use Slim\Http\Request;
    use Slim\Http\Response;

abstract class Base
{
    public function __invoke(Request $slim_request, Response $slim_response, array $args) : Response
    {
        $request_type = $slim_request->getMethod();
        if ($request_type === 'GET') {
            $parameters = $slim_request->getQueryParams();
        } else {
            $parameters = $slim_request->getParsedBody();
        }
        if ($parameters === null) {
            $parameters = [];
        }
        $response_contents = $this->handle($request_type, $parameters, $args);
        return $this->prepareResponse($response_contents, $slim_response);
    }

    protected function prepareResponse(array $response_contents, Response $slim_response) : Response
    {
        $response_contents = $this->setDefaultResponseContents($response_contents);
        return $slim_response->withJson($response_contents['json'])
            ->withStatus($response_contents['status']);
    }

    private function setDefaultResponseContents(array $response_contents) : array
    {
        if (!isset($response_contents['json'])) {
            $response_contents['json'] = [];
        }
        if (!isset($response_contents['status'])) {
            $response_contents['status'] = 403;
        }
        return $response_contents;
    }

    abstract public function handle(string $request_type, array $parameters, array $args) : array;
}
