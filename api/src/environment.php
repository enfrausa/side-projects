<?php
    // @codeCoverageIgnoreStart
    $env = \Dotenv\Dotenv::create(__DIR__ . '/../../private/');
    $env->load();
    $env->required([
        'ENVIRONMENT',
        'DATABASE_HOSTNAME',
        'DATABASE_DATABASE',
        'DATABASE_USERNAME',
        'DATABASE_PASSWORD',
    ]);
