<?php
    // @codeCoverageIgnoreStart
    namespace App\HealthCheck;

    use App\RouteHandler\HealthCheck\HealthCheck;
    use PHPUnit\Framework\TestCase;

final class HealthCheckTests extends TestCase
{
    /**
     * @dataProvider sampleRequests
     */
    public function testShouldOnlyAllowGet($request_type, $expected_status) : void
    {
        $handler = new HealthCheck();
        $response = $handler->handle($request_type, [], []);
        $this->assertEquals(
            $expected_status,
            $response['status'],
            'A request other than GET was allowed.'
        );
    }

    public function sampleRequests()
    {
        return [
            ['GET', 200],
            ['DELETE', 403],
            ['OPTIONS', 403],
            ['PATCH', 403],
            ['POST', 403],
        ];
    }
}
