## Install Steps
- `cd frontend`
- `npm install`
- `cd ../api/`
- `composer install`

## Rebuild Assets On-Save
- `cd frontend`
- `npm start`