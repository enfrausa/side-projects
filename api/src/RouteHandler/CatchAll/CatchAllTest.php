<?php
    // @codeCoverageIgnoreStart
    namespace App\CatchAll;

    use App\RouteHandler\CatchAll\CatchAll;
    use PHPUnit\Framework\TestCase;

final class CatchAllTests extends TestCase
{
    public function testShouldReturn403() : void
    {
        $handler = new CatchAll();
        $response = $handler->handle('random_request', [], []);
        $this->assertEquals(
            403,
            $response['status'],
            'Response did not return HTTP 403.'
        );
    }
}
