<?php
    // @codeCoverageIgnoreStart
    $container = $app->getContainer();
    
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($container['settings']['database'], 'default');
    $capsule->setAsGlobal();
    $capsule->bootEloquent();
    $container['database'] = function ($container) use ($capsule) {
        return $capsule;
    };
