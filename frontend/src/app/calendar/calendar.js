app.controller(
    'CalendarController',
    function($scope) {
        
        $scope.app = {
            calendar: {},
            init: function init() {
                console.log('init | start');
                year = moment().year();
                this.calendar[year] = this.fn.buildCalendarYear(year);
                console.log('init | done');
            },
            fn: {
                buildCalendarYear: function(year) {
                    var calendar_year = {};
                    this.fillCalendar(year, calendar_year);
                    return calendar_year;
                },
                fillDays: function(year, month) {
                    var calendar_month = [],
                        current_date = moment().year(year).month(month).startOf('month'),
                        end_date = moment().year(year).month(month).endOf('month');
                        while (current_date.isBefore(end_date)) {
                            calendar_month.push([{
                                chain: this.isChain(year, month, current_date.day()),
                                today: current_date.format('YYYY-MM-DD') === moment().format('YYYY-MM-DD'),
                            }]);
                            current_date.add(1, 'd');
                        }
                    return calendar_month;
                },
                fillCalendar: function(year, calendar_year) {
                    for (var month = 0; month < 12; month++) {
                        calendar_year[month] = this.fillDays(year, month);
                    }
                },
                formatMonthName: function(month_number) {
                    return moment().month(month_number).format('MMM');
                },
                formatDayName: function(day_number) {
                    return ("0" + (day_number + 1)).slice(-2);
                },
                isChain: function(year, month, day) {
                    var random = Math.random();
                    if (random > 0.5) {
                        return false;
                    }
                    console.log(year +' | '+ month +' | '+day);
                    return true;
                }
            },
        };

        $scope.app.init();
    }
);
