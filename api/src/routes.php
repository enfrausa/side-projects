<?php
    // @codeCoverageIgnoreStart
    use App\RouteHandler\CatchAll\CatchAll;
    use App\RouteHandler\HealthCheck\HealthCheck;

    $app->any('/health[/]', HealthCheck::class);

    $app->any('/[{path:.*}]', CatchAll::class);
