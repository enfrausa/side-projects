<?php
    namespace App\RouteHandler\HealthCheck;

    use App\RouteHandler\Base;

final class HealthCheck extends Base
{
    public function handle(string $request_type, array $parameters, array $args) : array
    {
        switch ($request_type) {
            case 'GET':
                return [
                    'json' => [
                        'success' => true,
                        'message' => 'All okay'
                    ],
                    'status' => 200,
                ];
                break;
            default:
                return [
                    'json' => [
                        'success' => false,
                        'error_message' => 'Access denied',
                        'request_type' => $request_type,
                    ],
                    'status' => 403,
                ];
                break;
        }
    }
}
