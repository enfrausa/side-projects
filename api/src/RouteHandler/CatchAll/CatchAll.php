<?php
    namespace App\RouteHandler\CatchAll;

    use App\RouteHandler\Base;

final class CatchAll extends Base
{
    public function handle(string $request_type, array $parameters, array $args) : array
    {
        // Switch is not required but is left to illustrate how to handle different request types
        // e.g. GET, DELETE, OPTIONS, PATCH, POST
        switch ($request_type) {
            default:
                return [
                    'json' => [
                        'success' => false,
                        'error_message' => 'Access denied',
                        'request_type' => $request_type,
                    ],
                    'status' => 403,
                ];
                break;
        }
    }
}
