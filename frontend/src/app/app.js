var api_url = 'http://api.enfrausa.com/';
var app = angular.module("application", ["ngCookies", "ngRoute"]);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/calendar", {
        controller: "CalendarController",
        templateUrl : "views/calendar.html"
    })
    // .when("/page", {
    //     controller: "PageController",
    //     templateUrl : "views/page.html"
    // })
    .otherwise({
        controller: "HomeController",
        templateUrl : "views/home.html"
    });
    $locationProvider.html5Mode(true);
});
